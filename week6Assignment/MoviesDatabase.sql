create database week6_haritha;

use week6_haritha;

create table MoviesComing(id int primary key,title varchar(20) ,releasedate varchar(20),year int,contentrating varchar(20));

insert into MoviesComing values(1,"TombRaider","2018-03-16",2018,"PG-13");
insert into MoviesComing values(2,"IsleOfDogs","2018-05-04",2018,"PG-13");
insert into MoviesComing values(3,"Souvenir","2017-05-19",2016,"BT1");
insert into MoviesComing values(4,"LeanOnPete","2018-05-04",2017,"R");
insert into MoviesComing values(5,"LifeOfParty","2018-05-11",2018,"PG-13");

select *from MoviesComing;

 create table MoviesInTheater(id int primary key,title varchar(20) ,releasedate varchar(20),year int,contentrating varchar(20));

insert into MoviesInTheater  values(1,"Wstern","2017-08-24",2017,"15");
 insert into MoviesInTheater  values(2,"Jumanji","2017-12-22",2017,"11");
 insert into MoviesInTheater  values(3,"BlackPanther","2018-02-14",2018,"15");
 insert into MoviesInTheater  values(4,"Loveless","2017-06-01",2017,"R");
 insert into MoviesInTheater  values(5,"TheParty","2018-01-05",2017,"11");


select * from MoviesInTheater;

create table TopRatedIndia(title varchar(20) ,releasedate varchar(20),year int,imdbrating varchar(20),duration varchar(10));

insert into TopRatedIndia values("Drishyam","2013-02-09",2013,"8.9","PT160M");
insert into TopRatedIndia values("GolMaal","1979-04-20",1979,"8.7","PT144M");
insert into TopRatedIndia values("TaareZameenPar","2008-12-26",2007,"8.5","PT165M");
insert into TopRatedIndia values("Thalvar","2015-10-02",2015,"8.3","PT132M");
insert into TopRatedIndia values("PK","2014-12-09",2014,"8.2","PT153M");

 select * from TopRatedIndia;

create table TopRatedMovies(title varchar(20) ,releasedate varchar(20),year int,imdbrating varchar(20),duration varchar(10));

insert into TopRatedMovies  values("24","2016-05-06",2016,"8","PT164M");
insert into TopRatedMovies values("HighWay","2014-02-11",2014,"7.6","PT133M");
insert into TopRatedMovies values("Khakee","2004-01-23",2004,"7.5","PT174M");
insert into TopRatedMovies values("Yuval","2004-05-22",2004,"7.8","PT161M");
insert into TopRatedMovies values("LifeInMetro","2007-05-11",2007,"7.4","PT132M");

select * from TopRatedMovies;

create table Favourite(id int primary key,title varchar(20) ,releasedate varchar(20),year int);

insert into Favourite Values(1,"Hannah","2018-01-24",2017);
insert into Favourite Values(2,"GamesNight","2018-02-28",2018);
insert into Favourite Values(3,"Areax","2018-03-23",2018);

select * from Favourite;
