package com.greatlearning.week6.dao;

import java.sql.Connection;


import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.ArrayList;
import java.util.List;

import com.greatlearning.week6.bean.MoviesComing;
import com.greatlearning.week6.resource.DbResource;

public class MoviesComingDao {

	public static List<MoviesComing> findAllMoviesComing() {
		List<MoviesComing> listOfMoviesComing = new ArrayList<>();
		try {
			//Class.forName("com.mysql.cj.jdbc.Driver");
			//Connection con = DriverManager.getConnection("jdbc:mysql://localhost:3306/week6_haritha","root","root");
			Connection con = DbResource.getDbConnection ();
			PreparedStatement pstmt = con.prepareStatement("select * from MoviesComing");
			ResultSet rs = pstmt.executeQuery();
			while(rs.next()) {
					MoviesComing mc = new MoviesComing();
					mc.setId(rs.getInt(1));
					mc.setTitle(rs.getString(2));
					mc.setContentRating(rs.getString(3));
					mc.setYear(rs.getInt(4));
					listOfMoviesComing.add(mc);
			}
			} catch (Exception e) {
				System.out.println("In delete method "+e);
			}
		return listOfMoviesComing;
	}

	
}
