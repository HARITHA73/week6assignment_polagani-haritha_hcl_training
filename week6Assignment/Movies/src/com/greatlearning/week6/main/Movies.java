package com.greatlearning.week6.main;

import java.sql.SQLException;



import java.util.List;

import com.greatlearning.week6.bean.Favourite;
import com.greatlearning.week6.bean.MoviesComing;
import com.greatlearning.week6.bean.MoviesInTheater;
import com.greatlearning.week6.bean.TopRatedIndia;
import com.greatlearning.week6.bean.TopRatedMovies;
import com.greatlearning.week6.dao.FavouriteDao;
import com.greatlearning.week6.dao.MoviesComingDao;
import com.greatlearning.week6.dao.MoviesInTheaterDao;
import com.greatlearning.week6.dao.TopRatedIndiaDao;
import com.greatlearning.week6.dao.TopRatedMoviesDao;

 
	public class Movies {  
	
	  
	    public static void main(String[] args)
	    {
	        // read All
	      List<MoviesComing> mc = MoviesComingDao.findAllMoviesComing();
	       for (MoviesComing allMoviesComing : mc) {
	          System.out.println(allMoviesComing);
	        }
	  
	       List<MoviesInTheater> mit = MoviesInTheaterDao.findAllMovies();
	       for (MoviesInTheater allMoviesInTheater : mit) {
	          System.out.println(allMoviesInTheater);
	        }
	       
	       List<TopRatedIndia> tri = TopRatedIndiaDao.findAllMovies();
	       for (TopRatedIndia allTopRatedIndia : tri) {
	          System.out.println(allTopRatedIndia);
	        }
	       
	       List<TopRatedMovies> trm = TopRatedMoviesDao.findAllMovies();
	       for (TopRatedMovies allTopRatedMovies : trm) {
	          System.out.println(allTopRatedMovies);
	        }
	       
	       List<Favourite> f = FavouriteDao.findAllMovies();
	       for (Favourite allFavourite : f) {
	          System.out.println(allFavourite);
	        }
	    
	   	}
	

	    }	
