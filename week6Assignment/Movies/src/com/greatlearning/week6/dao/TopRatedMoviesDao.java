package com.greatlearning.week6.dao;

import java.sql.Connection;


import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.ArrayList;
import java.util.List;

import com.greatlearning.week6.bean.TopRatedMovies;
import com.greatlearning.week6.resource.DbResource;

public class TopRatedMoviesDao {

	public static List<TopRatedMovies> findAllMovies() {
		List<TopRatedMovies> listOfTopRatedMovies = new ArrayList<>();
		try {
			//Class.forName("com.mysql.cj.jdbc.Driver");
			//Connection con = DriverManager.getConnection("jdbc:mysql://localhost:3306/week6_haritha","root","root");
			Connection con = DbResource.getDbConnection ();
			PreparedStatement pstmt = con.prepareStatement("select * from TopRatedMovies");
			ResultSet rs = pstmt.executeQuery();
			while(rs.next()) {
					TopRatedMovies trm = new TopRatedMovies();
					trm.setYear(rs.getInt(3));
				    trm.setTitle(rs.getString(1));
				    trm.setReleaseDate(rs.getString(2));
					trm.setImbdRating(rs.getString(4));
					trm.setDuration(rs.getString(5));
					listOfTopRatedMovies.add(trm);
			}
			} catch (Exception e) {
				System.out.println("In delete method "+e);
			}
		return listOfTopRatedMovies;
	}
	
}
