package com.greatlearning.week6.dao;

import java.sql.Connection;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.ArrayList;
import java.util.List;

import com.greatlearning.week6.bean.TopRatedIndia;
import com.greatlearning.week6.resource.DbResource;

public class TopRatedIndiaDao {

	public static List<TopRatedIndia> findAllMovies() {
		List<TopRatedIndia> listOfTopRatedIndia = new ArrayList<>();
		try {
			//Class.forName("com.mysql.cj.jdbc.Driver");
			//Connection con = DriverManager.getConnection("jdbc:mysql://localhost:3306/week6_haritha","root","root");
			Connection con = DbResource.getDbConnection ();
			PreparedStatement pstmt = con.prepareStatement("select * from TopRatedIndia");
			ResultSet rs = pstmt.executeQuery();
			while(rs.next()) {
					TopRatedIndia tri = new TopRatedIndia();
					tri.setYear(rs.getInt(3));
				    tri.setTitle(rs.getString(1));
				    tri.setReleaseDate(rs.getString(3));
					tri.setImbdRating(rs.getString(4));
					tri.setDuration(rs.getString(5));
					listOfTopRatedIndia.add(tri);
			}
			} catch (Exception e) {
				System.out.println("In delete method "+e);
			}
		return listOfTopRatedIndia;
	}
}
