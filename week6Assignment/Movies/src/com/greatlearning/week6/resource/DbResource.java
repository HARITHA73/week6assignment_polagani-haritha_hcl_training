package com.greatlearning.week6.resource;


import java.sql.Connection;


import java.sql.DriverManager;
import java.sql.SQLException;
public class DbResource  {

		// database connection using singleton
	static Connection con;
	public static Connection getDbConnection() {
		try {
			Class.forName("com.mysql.cj.jdbc.Driver");
			con = DriverManager.getConnection("jdbc:mysql://localhost:3306/week6_haritha"
				, "root", "root");
			return  con;
		} catch (Exception e) {
			System.out.println("db Connection method "+e);
		}
		return null;
	}
}