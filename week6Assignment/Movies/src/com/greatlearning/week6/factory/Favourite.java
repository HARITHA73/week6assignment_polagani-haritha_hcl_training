package com.greatlearning.week6.factory;

import com.greatlearning.week6.movies.InterfaceMovies;

public abstract class Favourite implements InterfaceMovies {

	public void display() {
		System.out.println("Favourite display() method");
	}

}
