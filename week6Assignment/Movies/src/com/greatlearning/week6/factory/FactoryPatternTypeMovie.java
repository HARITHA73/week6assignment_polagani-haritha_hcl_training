package com.greatlearning.week6.factory;

import com.greatlearning.week6.bean.Favourite;

import com.greatlearning.week6.bean.MoviesComing;
import com.greatlearning.week6.bean.MoviesInTheater;
import com.greatlearning.week6.bean.TopRatedIndia;
import com.greatlearning.week6.bean.TopRatedMovies;
import com.greatlearning.week6.movies.InterfaceMovies;

public class FactoryPatternTypeMovie {
	
	public InterfaceMovies getInterfcaeMovies(String movieType) {

		if (movieType == null) {
			return null;
		}
		if (movieType.equalsIgnoreCase("MOVIESCOMING")) {
			return (InterfaceMovies) new MoviesComing();
		}
		else if (movieType.equalsIgnoreCase("MOVIESINTHEATER")) {
			return (InterfaceMovies) new MoviesInTheater();
		} 
		else if (movieType.equalsIgnoreCase("TOPRATEDINDIA")) {
			return (InterfaceMovies) new TopRatedIndia();
		} 
		else if (movieType.equalsIgnoreCase("TOPRATEDMOVIES")) {
			return (InterfaceMovies) new TopRatedMovies();
		}
		else if (movieType.equalsIgnoreCase("Favourite")) {
			return (InterfaceMovies) new Favourite();
	    }
		return null;
}
}

