package com.greatlearning.week6.dao;

import java.sql.Connection;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.ArrayList;
import java.util.List;

import com.greatlearning.week6.bean.MoviesInTheater;
import com.greatlearning.week6.resource.DbResource;

public class MoviesInTheaterDao {

	public static List<MoviesInTheater> findAllMovies() {
		List<MoviesInTheater> ListOfMoviesInTheater = new ArrayList<>();
		try {
			//Class.forName("com.mysql.cj.jdbc.Driver");
			//Connection con = DriverManager.getConnection("jdbc:mysql://localhost:3306/week6_haritha","root","root");
			Connection con = DbResource.getDbConnection ();
			PreparedStatement pstmt = con.prepareStatement("select * from MoviesInTheater");
			ResultSet rs = pstmt.executeQuery();
			while(rs.next()) {
				MoviesInTheater mit = new MoviesInTheater();
					mit.setId(rs.getInt(1));
					mit.setTitle(rs.getString(2));
					mit.setContentRating(rs.getString(3));
					mit.setYear(rs.getInt(4));
					ListOfMoviesInTheater.add(mit);
			}
			} catch (Exception e) {
				System.out.println("In delete method "+e);
			}
		return ListOfMoviesInTheater;
		
}
}
