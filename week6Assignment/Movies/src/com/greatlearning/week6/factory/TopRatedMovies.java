package com.greatlearning.week6.factory;

import com.greatlearning.week6.movies.InterfaceMovies;

public abstract class TopRatedMovies implements InterfaceMovies {

	public void display() {
		System.out.println("TopRatedMovies display() method");
	}

}
