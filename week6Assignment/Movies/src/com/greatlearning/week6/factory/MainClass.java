package com.greatlearning.week6.factory;

import com.greatlearning.week6.movies.InterfaceMovies;

public class MainClass {

	   public static void main(String[] args) throws Exception{
		
		FactoryPatternTypeMovie movieFactory = new FactoryPatternTypeMovie();
		
	      
	    InterfaceMovies moviesComing = movieFactory.getInterfcaeMovies("MOVIESCOMMING");

	    
	    moviesComing.dispaly();
	      
	    InterfaceMovies moviesInTheaters = movieFactory.getInterfcaeMovies("MOVIESINTHEATRES");
		
	    moviesInTheaters.dispaly();
	      
	    InterfaceMovies topRatedMoviesIndia = movieFactory.getInterfcaeMovies("TOPRATEDINDIA");
			
	    topRatedMoviesIndia.dispaly();
	      
	    InterfaceMovies topRatedMovies = movieFactory.getInterfcaeMovies("TOPRATEDMOVIES");
			
	    topRatedMovies.dispaly();
	    }
}
