package com.greatlearning.week6.dao;

import java.sql.Connection;


import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.ArrayList;
import java.util.List;

import com.greatlearning.week6.bean.Favourite;
import com.greatlearning.week6.resource.DbResource;
public class FavouriteDao{

public static  List<Favourite> findAllMovies() {
	List<Favourite> listOfFavourite = new ArrayList<>();
	try {
		//Class.forName("com.mysql.cj.jdbc.Driver");
		//Connection con = DriverManager.getConnection("jdbc:mysql://localhost:3306/week6_haritha","root","root");
		Connection con = DbResource.getDbConnection ();
		PreparedStatement pstmt = con.prepareStatement("select * from Favourite");
		ResultSet rs = pstmt.executeQuery();
		while(rs.next()) {
			Favourite f = new Favourite();
				f.setId(rs.getInt(1));
				f.setTitle(rs.getString(2));
				f.setReleaseDate(rs.getString(3));
				f.setYear(rs.getInt(4));
				listOfFavourite.add(f);
		}
		} catch (Exception e) {
			System.out.println("In delete method "+e);
		}
	return listOfFavourite;
}
}



