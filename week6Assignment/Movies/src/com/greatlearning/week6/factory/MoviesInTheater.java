package com.greatlearning.week6.factory;
import com.greatlearning.week6.movies.InterfaceMovies;
public abstract class  MoviesInTheater implements InterfaceMovies {
	public void display() {
		System.out.println("MoviesInTheater display() method");
	}

}
